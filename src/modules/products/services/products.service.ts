import {
  BadRequestException,
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { UsersService } from '../../users/services';
import { getChange } from '../../../utils/helper';
import { BuyProductDto, CreateProductDto, UpdateProductDto } from '../dto';
import { Product } from '../entities/product.entity';
import { getMongoRepository } from 'typeorm';

@Injectable()
export class ProductsService {
  private productRepository = getMongoRepository(Product);

  constructor(private userService: UsersService) {}

  async create(user, createProductDto: CreateProductDto) {
    const newProduct = this.productRepository.create(createProductDto);
    newProduct.user = user.id;
    const product = await this.productRepository.save(newProduct);
    delete product.user;
    return {
      success: true,
      data: product,
    };
  }

  async findAll() {
    const products = await this.productRepository.find();
    return { success: true, products };
  }

  async findOne(id: number) {
    try {
      const product = await this.productRepository.findOne({ id });
      if (!product) {
        throw Error();
      }
      return {
        success: true,
        data: product,
      };
    } catch (error) {
      throw new HttpException('No record found', HttpStatus.BAD_REQUEST);
    }
  }

  async update(user, id, updateProductDto: UpdateProductDto) {
    const updates = Object.keys(updateProductDto);
    try {
      const product = await this.productRepository.findOne({
        where: { id },
        relations: ['user'],
      });

      if (!product) throw new BadRequestException('No product like this');

      if (user.id !== product.user.id)
        throw new ForbiddenException('Whoops, error occured');

      updates.forEach((update) => (product[update] = updateProductDto[update]));

      delete product.user;

      const updateProduct = await this.productRepository.updateOne(
        { id },
        product,
      );
      return {
        message: 'product updated successfully',
        updateProduct,
      };
    } catch (error) {
      throw error;
    }
  }

  async remove(user, id: number) {
    try {
      const product = await this.productRepository.findOne({
        where: { id: id },
        relations: ['user'],
      });

      if (user.id !== product.user.id)
        throw new ForbiddenException('Whoops, error occured');

      const data = await this.productRepository.deleteOne({ id });

      if (!data) {
        throw new BadRequestException('No record found');
      }
      return {
        success: true,
        message: 'Product deleted successfully',
      };
    } catch (error) {
      throw error;
    }
  }

  async addProductToCart(user, buyProductDto: BuyProductDto) {
    const items = buyProductDto.products;
    let total_amount = 0;
    const products_output_array = [];
    try {
      // get current users deposit
      if (user.deposit <= 0)
        throw new BadRequestException(
          'Whoops! you dont have any coins deposited to make purchase',
        );
      for await (const item of items) {
        const product = await this.productRepository.findOne(item.product_id);
        if (product.quantity > item.quantity) {
          const amount = item.quantity * product.amount;
          total_amount = total_amount + amount;
          product.quantity -= item.quantity;
          await this.productRepository.create(product);
          const productBought = {
            id: product.id,
            name: product.name,
            amount: product.amount,
            qauntity: item.quantity,
          };
          products_output_array.push(productBought);
        }
      }

      if (!total_amount || products_output_array.length === 0) {
        throw new BadRequestException(
          'Whoops! something went wrong, re-select products and try again',
        );
      }

      const changeLeft = user.deposit - total_amount;
      if (changeLeft < 0)
        throw new BadRequestException(
          'The total price of the selected products cannot be more than your deposit',
        );

      // reset the users deposit to zero
      await this.userService.resetDeposit(user);
      return {
        success: true,
        data: {
          Products: products_output_array,
          total_amount,
          change: getChange(changeLeft),
        },
      };
    } catch (error) {
      throw error;
    }
  }
}
