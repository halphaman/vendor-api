// import { Test, TestingModule } from '@nestjs/testing';
// import { getRepositoryToken } from '@nestjs/typeorm';
// import { UsersService } from '../../users/services';
// import { CreateProductDto } from '../dto';
// import { Product } from '../entities/product.entity';
// import { ProductsService } from './products.service';

// describe('ProductService', () => {
//   let service: ProductsService;
//   const userService = {};

//   const mockProduct = new Product();
//   mockProduct.name = 'product1';
//   mockProduct.description = 'product description';
//   mockProduct.quantity = 10;
//   mockProduct.amount = 50.0;

//   const productRepository = {
//     findOne: jest.fn((id) => {
//       return {
//           id: id,
//           ...mockProduct,
//         },
//       };
//     }),
//   };

//   beforeEach(async () => {
//     const module: TestingModule = await Test.createTestingModule({
//       providers: [
//         ProductsService,
//         {
//           provide: getRepositoryToken(Product),
//           useValue: productRepository,
//         },
//         {
//           provide: UsersService,
//           useValue: userService,
//         },
//       ],
//     }).compile();

//     service = module.get<ProductsService>(ProductsService);
//   });

//   it('should be defined', () => {
//     expect(service).toBeDefined();
//   });

//   it('should find on product for a given Id', async () => {
//     expect(await service.findOne(12)).toEqual({
//       sucess: true,
//       product: {
//         id: 12,
//         ...mockProduct,
//       },
//     });
//   });
// });
