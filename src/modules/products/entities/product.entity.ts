import { User } from '../../users/entities/user.entity';
import { BaseEntity } from '../../../shared/entities';
import { Column, Entity, JoinColumn, ManyToOne, ObjectIdColumn } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@Entity('products')
@ObjectType()
export class Product extends BaseEntity {
  @Field()
  @ObjectIdColumn()
  id;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column()
  description: string;

  @ManyToOne(() => User, (user) => user.products)
  @JoinColumn({ name: 'seller_id' })
  user: User;

  @Field()
  @Column()
  quantity: number;

  @Field()
  @Column({ type: 'decimal', precision: 20, scale: 2 })
  amount: number;
}
