import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { ProductsController } from './controllers';
import { Product } from './entities/product.entity';
import { ProductsService } from './services';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Product]), UsersModule],
  controllers: [ProductsController],
  providers: [ProductsService, ProductsController],
})
export class ProductsModule {}
