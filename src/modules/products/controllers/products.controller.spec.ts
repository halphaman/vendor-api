import { Test, TestingModule } from '@nestjs/testing';
import { ProductsController } from './products.controller';
import { ProductsService } from '../services/products.service';
import { CreateProductDto } from '../dto';

describe('ProductsController', () => {
  let controller: ProductsController;
  let mockedService: ProductsService;

  const createDto = new CreateProductDto();
  createDto.name = 'product1';
  createDto.description = 'product description';
  createDto.quantity = 10;
  createDto.amount = 50.0;

  const productsService = {
    create: jest.fn((user, createDto) => {
      return {
        success: true,
        product: { id: 1, ...createDto },
      };
    }),
  };

  const req = jest.fn((x) => x);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [
        {
          provide: ProductsService,
          useValue: productsService,
        },
      ],
    }).compile();

    controller = module.get<ProductsController>(ProductsController);
    mockedService = module.get<ProductsService>(ProductsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should have create function', () => {
    expect(controller.create).toBeDefined();
  });

  it('should create new product and return with id', async () => {
    expect(await controller.create(req, createDto)).toEqual({
      success: true,
      product: { id: 1, ...createDto },
    });
    expect(mockedService.create).toBeCalled();
  });
});
