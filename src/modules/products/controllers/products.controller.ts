import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
} from '@nestjs/common';
import { ProductsService } from '../services';
import { BuyProductDto, CreateProductDto, UpdateProductDto } from '../dto';
import { RequestWithUser } from 'src/common/interface';
import { UserRole } from '../../../common/enum';
import { Roles } from '../../../common/decorators/roles.decorator';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Product } from '../entities/product.entity';

@Resolver('products')
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Roles(UserRole.Seller)
  @Post('/new')
  @Mutation(() => Product)
  async create(@Req() req, @Body() createProductDto: CreateProductDto) {
    return await this.productsService.create(req.user, createProductDto);
  }

  @Roles(UserRole.Buyer)
  @Post('/buy')
  @Mutation(() => Product)
  async buyProduct(@Req() req, @Body() buyProductDto: BuyProductDto) {
    return await this.productsService.addProductToCart(req.user, buyProductDto);
  }

  @Get()
  @Query(() => Product)
  async findAll() {
    return await this.productsService.findAll();
  }

  @Get(':id')
  @Query(() => Product)
  async findOne(@Args('id') @Param('id') id: string) {
    return await this.productsService.findOne(+id);
  }

  @Roles(UserRole.Seller)
  @Patch(':id')
  @Mutation(() => Product)
  async update(
    @Req() req: RequestWithUser,
    @Args('id')
    @Param('id')
    id: string,
    @Body() updateProductDto: UpdateProductDto,
  ) {
    return await this.productsService.update(req.user, +id, updateProductDto);
  }

  @Roles(UserRole.Seller)
  @Delete(':id')
  @Mutation(() => Product)
  async remove(
    @Req() req: RequestWithUser,
    @Args('id')
    @Param('id')
    id: string,
  ) {
    return await this.productsService.remove(req.user, +id);
  }
}
