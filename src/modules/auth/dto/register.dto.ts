import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class RegisterDto {
  @IsEmail()
  @IsString()
  email: string;

  @Field()
  @IsNotEmpty()
  @IsString({ message: 'Invalid character' })
  username: string;

  @Field()
  @IsString()
  password: string;

  @Field()
  @IsOptional()
  roles: string;
}
