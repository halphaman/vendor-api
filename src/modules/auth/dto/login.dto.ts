import { IsNotEmpty, IsString } from 'class-validator';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class LoginDto {
  @Field()
  @IsNotEmpty()
  @IsString({ message: 'Invalid character' })
  username: string;

  @Field()
  @IsString()
  password: string;
}
