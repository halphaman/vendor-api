import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Public } from 'src/common/decorators';
import { RtGuard } from 'src/common/guards';
import { RequestTokenData, RequestWithUser } from 'src/common/interface';
import { RegisterDto } from '../dto/register.dto';
import { LocalAuthGuard } from '../local-auth.guard';
import { AuthService } from '../services';
import { Args, Mutation, Query } from '@nestjs/graphql';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @Mutation()
  @Post('/register')
  async localRegister(@Body() registerDto: RegisterDto) {
    return await this.authService.register(registerDto);
  }

  @Public()
  @Mutation()
  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async localLogin(@Req() request: RequestWithUser) {
    return request.user;
  }

  @HttpCode(HttpStatus.OK)
  @Mutation()
  @Post('/logout')
  async logout(@Req() req: RequestTokenData) {
    const { userId } = req.user;
    return this.authService.logout(userId);
  }

  @Public()
  @HttpCode(HttpStatus.OK)
  @UseGuards(RtGuard)
  @Mutation()
  @Post('/refresh')
  async refreshTokens(@Req() req: RequestTokenData) {
    const user = req.user;
    return await this.authService.refreshTokens(user.userId, user.refreshToken);
  }

  @HttpCode(HttpStatus.OK)
  @Get('/me')
  @Query('/me')
  async profile(@Args() @Req() req: RequestTokenData) {
    return req.user;
  }
}
