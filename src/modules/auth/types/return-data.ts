export type UserFormat = {
  success: boolean;
  data: any;
  token: string;
};
