import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/modules/users/entities/user.entity';
import { UsersService } from 'src/modules/users/services';
import { checkData, hashData } from 'src/utils/helper';
import { getRepository } from 'typeorm';
import { Tokens } from '../types';
import { RegisterDto } from '../dto/register.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async register(registerData: RegisterDto) {
    try {
      const newUser = await this.usersService.create(registerData);
      const tokens = await this.getTokens(newUser.id, newUser.email);
      await this.updateRefreshToken(newUser.id, tokens.refresh_token);
      return { success: true, data: newUser, token: tokens.access_token };
    } catch (error) {
      if (error && error.code === 'ER_DUP_ENTRY') {
        throw new HttpException(
          'User with that email already exists',
          HttpStatus.BAD_REQUEST,
        );
      }
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async getAuthenticatedUser(username: string, plainTextPassword: string) {
    try {
      console.log({ username, plainTextPassword });
      const data = await this.usersService.findByUserName(username);
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, hashed_rt, ...user } = data;
      const isPasswordMatching = await checkData(plainTextPassword, password);

      if (!isPasswordMatching) {
        throw new HttpException('Access Denied', HttpStatus.FORBIDDEN);
      }
      const tokens = await this.getTokens(user.id, user.username);
      await this.updateRefreshToken(user.id, tokens.refresh_token);
      return { success: true, data: user, token: tokens.access_token };
    } catch (e) {
      throw new HttpException('Access Denied', HttpStatus.FORBIDDEN);
    }
  }

  async logout(userId: number) {
    await getRepository(User).save({
      id: userId,
      hashed_rt: null,
    });
  }

  async getTokens(userId: number, username: string): Promise<Tokens> {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        { userId, username },
        {
          secret: this.configService.get('jwt_secret'),
          expiresIn: 60 * 15,
        },
      ),
      this.jwtService.signAsync(
        { userId, username },
        {
          secret: this.configService.get('jwt_refresh_secret'),
          expiresIn: 60 * 60 * 24 * 7,
        },
      ),
    ]);

    return {
      access_token: accessToken,
      refresh_token: refreshToken,
    };
  }

  async updateRefreshToken(userId: number, refreshToken: string) {
    const hash = await hashData(refreshToken);
    const userRepo = getRepository(User);
    await userRepo.save({
      id: userId,
      hashed_rt: hash,
    });
  }

  async refreshTokens(username: number, refreshToken: string) {
    const user = await this.usersService.findByUserName(username);
    if (!user) throw new ForbiddenException('Access Denied');

    const isValidToken = await checkData(refreshToken, user.hashed_rt);

    if (!isValidToken) throw new ForbiddenException('Access Denied');
    const tokens = await this.getTokens(user.id, user.username);

    await this.updateRefreshToken(user.id, tokens.refresh_token);
    return tokens;
  }
}
