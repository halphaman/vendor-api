import { Module } from '@nestjs/common';
import { RolesService } from './services';
import { RolesController } from './controllers';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from './entities/role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Role])],
  controllers: [RolesController],
  providers: [RolesService, RolesController],
  exports: [RolesService],
})
export class RolesModule {}
