import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { RolesService } from '../services';
import { CreateRoleDto } from '../dto';
import { Roles } from 'src/common/decorators/roles.decorator';
import { UserRole } from 'src/common/enum';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Role } from '../entities/role.entity';

@Controller('roles')
@Resolver('roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @Roles(UserRole.Seller)
  @Mutation(() => Role)
  @Post('/new')
  create(@Body() createRoleDto: CreateRoleDto) {
    return this.rolesService.create(createRoleDto);
  }

  @Roles(UserRole.Seller)
  @Get()
  @Query(() => Role)
  findAll() {
    return this.rolesService.findAll();
  }

  @Roles(UserRole.Seller)
  @Delete(':id')
  @Mutation()
  remove(
    @Args('id')
    @Param('id')
    id: string,
  ) {
    return this.rolesService.remove(+id);
  }
}
