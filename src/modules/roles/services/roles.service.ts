import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { getMongoRepository } from 'typeorm';
import { CreateRoleDto } from '../dto';
import { Role } from '../entities/role.entity';

@Injectable()
export class RolesService {
  private roleRepository = getMongoRepository(Role);

  async create(createRoleDto: CreateRoleDto) {
    const newRole = await this.roleRepository.create(createRoleDto);
    const role = await this.roleRepository.save(newRole);
    return {
      success: true,
      data: role,
    };
  }

  async findAll() {
    return await this.roleRepository.find();
  }

  async findOne({ slug }) {
    return await this.roleRepository.findOne({
      where: { slug: slug },
    });
  }

  async remove(id: number) {
    try {
      const data = await this.roleRepository.delete({ id });
      if (!data.affected) {
        throw new Error();
      }
      return {
        success: true,
        message: 'Role deleted successfully',
      };
    } catch (error) {
      throw new HttpException('No record found', HttpStatus.BAD_REQUEST);
    }
  }
}
