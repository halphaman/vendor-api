import { UserRole } from 'src/common/enum';

export class CreateRoleDto {
  slug: string;
  name: UserRole;
}
