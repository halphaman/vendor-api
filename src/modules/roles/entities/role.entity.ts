import { UserRole } from '../../../common/enum';
import { User } from '../../users/entities/user.entity';
import { BaseEntity } from '../../../shared/entities';
import { Column, Entity, ManyToMany, ObjectIdColumn } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@Entity('roles')
@ObjectType()
export class Role extends BaseEntity {
  @ObjectIdColumn()
  id;

  @Field()
  @Column({ unique: true })
  slug: string;

  @Field()
  @Column({
    type: 'enum',
    enum: UserRole,
  })
  name: UserRole;

  @ManyToMany(() => User, (user) => user.roles)
  user: User;
}
