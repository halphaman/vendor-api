import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RolesService } from '../../roles/services';
import { creditAccount, hashData } from '../../../utils/helper';
import { Repository } from 'typeorm';
import { UpdateUserDto } from '../dto';
import { User } from '../entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private roleService: RolesService,
  ) {}

  async create(createUserDto) {
    const { role, ...other } = createUserDto;

    const slug = role === '' || !role ? { slug: 'buyer' } : { slug: role };
    const userRole = await this.roleService.findOne({ slug });

    if (!userRole) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }

    other.password = await hashData(other.password);
    other.roles = [userRole];

    const newUser = await this.userRepository.save(other);

    delete newUser.password;
    delete newUser.hashed_rt;

    return newUser;
  }

  findAll() {
    return this.userRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  async findByUserName(name) {
    return await this.userRepository.findOne({
      where: { username: name },
      relations: ['roles'],
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const updates = Object.keys(updateUserDto);
    try {
      const user = await this.userRepository.findOne({ id });

      if (!user) console.log('no product like this');

      updates.forEach((update) => (user[update] = updateUserDto[update]));

      const updatedUser = await this.userRepository.save({ ...user });

      return {
        message: 'profile updated successfully',
        data: updatedUser,
      };
    } catch (error) {
      console.log(error);
    }
  }

  async remove(id: number) {
    try {
      const data = await this.userRepository.delete({ id });
      if (!data.affected) {
        throw new Error();
      }
      return {
        success: true,
        message: 'Product deleted successfully',
      };
    } catch (error) {
      throw new HttpException('No record found', HttpStatus.BAD_REQUEST);
    }
  }

  // deposit
  async deposit(user, payload) {
    try {
      const { status, amount } = creditAccount(payload.amount);
      if (!status) {
        console.log('return back money');
      }
      const balance = parseInt(user.deposit) + amount;

      await this.userRepository.update(user.id, {
        deposit: balance,
      });

      return { message: 'deposit successful' };
    } catch {}
  }

  async resetDeposit(user) {
    const userData = user;
    userData.deposit = 0;
    return await this.userRepository.save(userData);
  }
}
