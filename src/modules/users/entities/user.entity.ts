import { Product } from '../../../modules/products/entities/product.entity';
import { Role } from '../../../modules/roles/entities/role.entity';
import { BaseEntity } from '../../../shared/entities';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ObjectIdColumn,
  OneToMany,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@Entity('users')
@ObjectType()
export class User extends BaseEntity {
  @ObjectIdColumn()
  id: number;
  @Field()
  @Column({ unique: true })
  email: string;

  @Field()
  @Column({ unique: true })
  username: string;

  @Field()
  @Column()
  password: string;

  @Field()
  @Column({ type: 'decimal', precision: 20, scale: 2, default: 0 })
  deposit: number;

  @Field()
  @Column({ nullable: true })
  hashed_rt: string;

  @OneToMany(() => Product, (product) => product.user)
  products: Product[];

  @ManyToMany(() => Role, (role) => role.user, { cascade: true })
  @JoinTable({})
  roles: Role[];
}
