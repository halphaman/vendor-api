import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
} from '@nestjs/common';
import { UsersService } from '../services';
import { DepositDto, UpdateUserDto } from '../dto';
import { Roles } from 'src/common/decorators/roles.decorator';
import { UserRole } from 'src/common/enum';
import { Mutation, Resolver } from '@nestjs/graphql';

@Controller('users')
@Resolver('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Roles(UserRole.Buyer)
  @Mutation('/deposit')
  @Post('/deposit')
  async depositCoin(@Req() req, @Body() depositDto: DepositDto) {
    const user = req.user;
    return await this.usersService.deposit(user, depositDto);
  }

  @Roles(UserRole.Buyer)
  @Post('/reset')
  async reseDeposit(@Req() req) {
    const user = req.user;
    return await this.usersService.resetDeposit(user);
  }

  @Roles(UserRole.Seller)
  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Roles(UserRole.Seller)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Roles(UserRole.Seller)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
