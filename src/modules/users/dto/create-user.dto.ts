import { IsString, IsNotEmpty, IsOptional, IsEmail } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString({ message: 'Invalid character' })
  username: string;

  @IsString()
  password: string;

  @IsOptional()
  roles: string;

  @IsOptional()
  hashed_rt: string;
}
