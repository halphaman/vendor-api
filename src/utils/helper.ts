import * as bcrypt from 'bcrypt';
import { config } from '../config';

export const creditAccount = (amount) => {
  const acceptedAmount = JSON.parse(config().deposit_range);

  const check = acceptedAmount.includes(amount);

  if (check) {
    return { status: true, amount: amount };
  } else {
    return { status: false, amount: amount };
  }
};

export const getChange = (change) => {
  const coins = JSON.parse(config().deposit_range);

  // sort the coin by the highest first
  const sortedCoin = coins.sort((a, b) => b - a);
  const coins_output = [];
  let amount = change;

  sortedCoin.map((coin) => {
    if (amount >= coin) {
      let counter_int = 0;
      const counter = Math.floor(amount / coin);
      while (counter_int < counter) {
        coins_output.push(coin);
        counter_int++;
      }
      amount = amount % coin;
    }
  });

  return coins_output.sort((a, b) => a - b);
};

export const hashData = async (payload) => {
  const saltOrRounds = 10;
  return await bcrypt.hash(payload, saltOrRounds);
};

export const checkData = async (payload, hash) => {
  return await bcrypt.compare(payload, hash);
};
