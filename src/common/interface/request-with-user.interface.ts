import { Request } from 'express';
import { User } from '../../modules/users/entities/user.entity';

export interface RequestWithUser extends Request {
  user: User;
}

export interface RequestTokenData extends Request {
  user: {
    userId: number;
    email: string;
    refreshToken?: string;
  };
}
