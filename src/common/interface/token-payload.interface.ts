interface TokenPayload {
  userId: number;
  email: string;
  refreshToken?: string;
}
