export const config = () => ({
  // JWT CONFIG
  jwt_secret: process.env.JWT_SECRET,
  jwt_refresh_secret: process.env.JWT_REFRESH_SECRET,
  jwt_expiration_time: process.env.JWT_EXPIRATION_TIME,

  // REMITA CONFIG
  remita_url: process.env.REMITA_URL,
  remita_apikey: process.env.REMITA_APIKEY,
  remita_service_type_id: process.env.REMITA_SERVICE_TYPE_ID,
  remita_merchant_id: process.env.REMITA_MERCHANT_ID,

  // PORT CONFIG
  port: parseInt(process.env.PORT, 10) || 3000,
  node_env: process.env.NODE_ENV,

  // coin deposit range
  deposit_range: process.env.DEPOSIT_RANGE,
});
