import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import helmet from 'helmet';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  /*app.use(
    helmet({
      contentSecurityPolicy:
        process.env.NODE_ENV === 'production' ? undefined : false,
    }),
  );*/
  app.use(
    helmet({ contentSecurityPolicy: false, crossOriginEmbedderPolicy: false }),
  );

  const configService = app.get(ConfigService);
  const port = configService.get('port');
  const currentEnv = configService.get('node_env');

  await app.listen(port, () =>
    console.log(
      `Server is currently run on ${currentEnv} environment in port ${port}`,
    ),
  );
}

bootstrap();
